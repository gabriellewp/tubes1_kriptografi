﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace StegoPict
{
    class Steganograph
    {
        public Steganograph() { 
        
        }
        private bool GetBit(byte b, short pos){
            return ((b & (byte)(1 << pos)) != 0);
        }
        private byte SetBit(byte b, short pos, bool newBit) {
            byte mask = (byte)(1 << pos);
            if (newBit) return (byte)(b | mask);
            else return (byte)(b & ~mask);
        }
        private int getKey (string key){
            int sum = 0;
            for (int i=0; i<key.Length; i++) sum += (int)key[i];
            return sum;
        }
        private long[] getInsertOrderArr(long bitmapLen, long key) {
            long[] ordertemp = new long[bitmapLen];
            for (long i = 0; i < ordertemp.Length; i++) ordertemp[i] = 0; 
            long idx = key % bitmapLen;
            for (long i = 1; i <= bitmapLen; i++) {
                while (ordertemp[idx] != 0) {
                    idx = (idx + 1) % bitmapLen;
                }
                ordertemp[idx] = i;
                idx = (idx + key) % bitmapLen;
            }
            long[] order = new long[bitmapLen];
            for (long i = 0; i < ordertemp.Length; i++) if (ordertemp[i] != 0) order[ordertemp[i] - 1] = i;
            return order;
        }
        public Bitmap Insert(Bitmap bmp, byte[] message, int bitLSB, string key) {
            //get the bitmap data into an array
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);
            int numbytes = bmpdata.Stride * bmp.Height;
            byte[] bytedata = new byte[numbytes];
            IntPtr ptr = bmpdata.Scan0;
            Marshal.Copy(ptr, bytedata, 0, numbytes); //bytedata is byte array of the image
            long[] order = getInsertOrderArr(bytedata.Length, getKey(key));
            //Let's insert allMessage inside bytedata
            long i = 0;
            foreach (byte b in message){
                for (short j = 0; j < 8; j++){
                    for (short k = 0; k < bitLSB; k++){
                        bytedata[order[i]] = SetBit(bytedata[order[i]], k, GetBit(b, j));
                        if (k != bitLSB - 1) j++;
                    }
                    i++;
                }
            }
            Marshal.Copy(bytedata, 0, ptr, numbytes);
            bmp.UnlockBits(bmpdata);
            return bmp;
        }
        public byte[] Extract(Bitmap bmp, int bitLSB, string key) {
            //get the bitmap data into an array
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);
            int numbytes = bmpdata.Stride * bmp.Height;
            byte[] bytedata = new byte[numbytes];
            IntPtr ptr = bmpdata.Scan0;
            Marshal.Copy(ptr, bytedata, 0, numbytes); //bytedata is byte array of the image
            long[] order = getInsertOrderArr(bytedata.Length, getKey(key));
            //Let's extract the byte inside the bmp
            //obtain the message length first
            byte[] messageLenByte = new byte[sizeof(int)];
            long i = 0;
            for (int j = 0; j < messageLenByte.Length; j++){
                for (short k = 0; k < 8; k++){
                    for (short l = 0; l < bitLSB; l++){
                        messageLenByte[j] = SetBit(messageLenByte[j], k, GetBit(bytedata[order[i]], l));
                        if (l != bitLSB - 1) k++;
                    }
                    i++;
                }
            }
            //obtain the whole message
            int messageLength = BitConverter.ToInt32(messageLenByte, 0);
            Console.WriteLine(messageLength);
            byte[] extractedMsg = new byte[messageLength];
            for (long j = 0; j < messageLength; j++){
                for (short k = 0; k < 8; k++){
                    for (short l = 0; l < bitLSB; l++){
                        extractedMsg[j] = SetBit(extractedMsg[j], k, GetBit(bytedata[order[i]], l));
                        if (l != bitLSB - 1) k++;
                    }
                    i++;
                }
            }
            // Unlock the bits.
            bmp.UnlockBits(bmpdata);
            return extractedMsg;
        }
        public double countPSNR(Bitmap b1, Bitmap b2) {
            if (b1.Height != b2.Height || b1.Width != b2.Width) //sizes are not equal, can't count it
                return -1;
            int h = b1.Height;
            int w = b2.Width;
            Rectangle rect = new Rectangle(0, 0, w, h);
            BitmapData picData1 = b1.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly, b1.PixelFormat);
            BitmapData picData2 = b2.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly, b2.PixelFormat);
            IntPtr ptr1 = picData1.Scan0;
            IntPtr ptr2 = picData2.Scan0;
            int byteCount = w * h;
            byte[] rgbValue1 = new byte[byteCount];
            byte[] rgbValue2 = new byte[byteCount];
            Marshal.Copy(ptr1, rgbValue1, 0, byteCount);
            Marshal.Copy(ptr2, rgbValue2, 0, byteCount);
            double sum = 0;
            for (int i = 0; i < byteCount; ++i)
                sum += Math.Pow(rgbValue1[i] - rgbValue2[i], 2);
            Marshal.Copy(rgbValue1, 0, ptr1, byteCount);
            Marshal.Copy(rgbValue2, 0, ptr2, byteCount);
            b1.UnlockBits(picData1);
            b2.UnlockBits(picData2);
            double rms = Math.Sqrt(sum / byteCount);
            double psnr = 20 * Math.Log10(256 / rms);
            return psnr;
        }
    }
}
