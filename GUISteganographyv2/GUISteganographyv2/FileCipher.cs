﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace StegoPict
{
    class FileCipher
    {
        private string key;
        public FileCipher() {
            key = null;
        }
        public FileCipher(string str) {
            key = str;
        }
        public void Encrypt(string input, string output) {
            using (Stream source = File.OpenRead(input)) //stream of source file
            using (Stream result = File.Create(output)) { //stream of result file
                byte[] byteKey = Encoding.ASCII.GetBytes(key);
                int inputByte = source.ReadByte();
                int keyIdx = 0;
                int tempByteInt = 0;
                while (inputByte != -1){
                    tempByteInt = (inputByte + (byte)byteKey[keyIdx]); //vigenere operation
                    if (tempByteInt > 255) tempByteInt -= 256;
                    result.WriteByte((byte)tempByteInt); //write character of byte
                    keyIdx++;
                    if (keyIdx == key.Length) keyIdx = 0;
                    inputByte = source.ReadByte();
                }
                source.Close();
                result.Close();
            }
        }
        public void Decrypt(string input, string output){
            using (Stream source = File.OpenRead(input)) //stream of source file
            using (Stream result = File.Create(output)) { //stream of result file
                byte[] byteKey = Encoding.ASCII.GetBytes(key);
                int inputByte = source.ReadByte();
                int keyIdx = 0;
                int tempByteInt = 0;
                while (inputByte != -1){
                    tempByteInt = (inputByte - (byte)byteKey[keyIdx]); //vigenere operation
                    if (tempByteInt < 0) tempByteInt += 256;
                    result.WriteByte((byte)tempByteInt); //write character of byte
                    keyIdx++;
                    if (keyIdx == key.Length) keyIdx = 0;
                    inputByte = source.ReadByte();
                }
                source.Close();
                result.Close();
            }
        }
    }
}
