﻿namespace StegoPict
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title = new System.Windows.Forms.Label();
            this.imgInsert = new System.Windows.Forms.TextBox();
            this.imgLabelInsert = new System.Windows.Forms.Label();
            this.getImgInsert = new System.Windows.Forms.Button();
            this.doInsert = new System.Windows.Forms.Button();
            this.openImgIns = new System.Windows.Forms.OpenFileDialog();
            this.keyStr = new System.Windows.Forms.TextBox();
            this.keyLabelInsert = new System.Windows.Forms.Label();
            this.msgLabelInsert = new System.Windows.Forms.Label();
            this.msgInsert = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.LSBoptIns = new System.Windows.Forms.ComboBox();
            this.isEncrypt = new System.Windows.Forms.CheckBox();
            this.getMessageInsert = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.LSBoptExt = new System.Windows.Forms.ComboBox();
            this.isDecrypt = new System.Windows.Forms.CheckBox();
            this.doExtract = new System.Windows.Forms.Button();
            this.imgExtract = new System.Windows.Forms.TextBox();
            this.keyLabelExtract = new System.Windows.Forms.Label();
            this.getImgExtract = new System.Windows.Forms.Button();
            this.keyExtract = new System.Windows.Forms.TextBox();
            this.imageLabelExtract = new System.Windows.Forms.Label();
            this.openFileMsgIns = new System.Windows.Forms.OpenFileDialog();
            this.openImageExt = new System.Windows.Forms.OpenFileDialog();
            this.labelPSNR = new System.Windows.Forms.Label();
            this.resImgInsertLabel = new System.Windows.Forms.Label();
            this.resImgInsertView = new System.Windows.Forms.PictureBox();
            this.oriImgInsertView = new System.Windows.Forms.PictureBox();
            this.oriImgInsertLabel = new System.Windows.Forms.Label();
            this.resultSaver = new System.Windows.Forms.Button();
            this.saveInsImage = new System.Windows.Forms.SaveFileDialog();
            this.saveExtFile = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resImgInsertView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oriImgInsertView)).BeginInit();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(179, 9);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(82, 20);
            this.title.TabIndex = 0;
            this.title.Text = "StegoPict";
            // 
            // imgInsert
            // 
            this.imgInsert.Location = new System.Drawing.Point(6, 34);
            this.imgInsert.Name = "imgInsert";
            this.imgInsert.Size = new System.Drawing.Size(264, 20);
            this.imgInsert.TabIndex = 1;
            // 
            // imgLabelInsert
            // 
            this.imgLabelInsert.AutoSize = true;
            this.imgLabelInsert.Location = new System.Drawing.Point(6, 18);
            this.imgLabelInsert.Name = "imgLabelInsert";
            this.imgLabelInsert.Size = new System.Drawing.Size(55, 13);
            this.imgLabelInsert.TabIndex = 2;
            this.imgLabelInsert.Text = "Image File";
            // 
            // getImgInsert
            // 
            this.getImgInsert.Location = new System.Drawing.Point(276, 34);
            this.getImgInsert.Name = "getImgInsert";
            this.getImgInsert.Size = new System.Drawing.Size(75, 23);
            this.getImgInsert.TabIndex = 3;
            this.getImgInsert.Text = "Browse";
            this.getImgInsert.UseVisualStyleBackColor = true;
            this.getImgInsert.Click += new System.EventHandler(this.onGetImageInsert);
            // 
            // doInsert
            // 
            this.doInsert.Location = new System.Drawing.Point(276, 129);
            this.doInsert.Name = "doInsert";
            this.doInsert.Size = new System.Drawing.Size(75, 23);
            this.doInsert.TabIndex = 4;
            this.doInsert.Text = "Insert";
            this.doInsert.UseVisualStyleBackColor = true;
            this.doInsert.Click += new System.EventHandler(this.onInsert);
            // 
            // openImgIns
            // 
            this.openImgIns.FileName = "imgIns";
            // 
            // keyStr
            // 
            this.keyStr.Location = new System.Drawing.Point(6, 129);
            this.keyStr.Name = "keyStr";
            this.keyStr.Size = new System.Drawing.Size(264, 20);
            this.keyStr.TabIndex = 6;
            // 
            // keyLabelInsert
            // 
            this.keyLabelInsert.AutoSize = true;
            this.keyLabelInsert.Location = new System.Drawing.Point(6, 113);
            this.keyLabelInsert.Name = "keyLabelInsert";
            this.keyLabelInsert.Size = new System.Drawing.Size(53, 13);
            this.keyLabelInsert.TabIndex = 7;
            this.keyLabelInsert.Text = "Seed Key";
            // 
            // msgLabelInsert
            // 
            this.msgLabelInsert.AutoSize = true;
            this.msgLabelInsert.Location = new System.Drawing.Point(6, 65);
            this.msgLabelInsert.Name = "msgLabelInsert";
            this.msgLabelInsert.Size = new System.Drawing.Size(69, 13);
            this.msgLabelInsert.TabIndex = 8;
            this.msgLabelInsert.Text = "Message File";
            // 
            // msgInsert
            // 
            this.msgInsert.Location = new System.Drawing.Point(6, 81);
            this.msgInsert.Name = "msgInsert";
            this.msgInsert.Size = new System.Drawing.Size(261, 20);
            this.msgInsert.TabIndex = 9;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(28, 42);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(378, 255);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.LSBoptIns);
            this.tabPage1.Controls.Add(this.isEncrypt);
            this.tabPage1.Controls.Add(this.getMessageInsert);
            this.tabPage1.Controls.Add(this.imgInsert);
            this.tabPage1.Controls.Add(this.doInsert);
            this.tabPage1.Controls.Add(this.keyLabelInsert);
            this.tabPage1.Controls.Add(this.msgLabelInsert);
            this.tabPage1.Controls.Add(this.getImgInsert);
            this.tabPage1.Controls.Add(this.msgInsert);
            this.tabPage1.Controls.Add(this.keyStr);
            this.tabPage1.Controls.Add(this.imgLabelInsert);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(370, 229);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Insert";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "LSB option";
            // 
            // LSBoptIns
            // 
            this.LSBoptIns.FormattingEnabled = true;
            this.LSBoptIns.Items.AddRange(new object[] {
            "1 bit",
            "2 bit",
            "3 bit"});
            this.LSBoptIns.Location = new System.Drawing.Point(6, 176);
            this.LSBoptIns.Name = "LSBoptIns";
            this.LSBoptIns.Size = new System.Drawing.Size(121, 21);
            this.LSBoptIns.TabIndex = 12;
            this.LSBoptIns.SelectedIndexChanged += new System.EventHandler(this.onLSBOptInsChanged);
            // 
            // isEncrypt
            // 
            this.isEncrypt.AutoSize = true;
            this.isEncrypt.Location = new System.Drawing.Point(144, 178);
            this.isEncrypt.Name = "isEncrypt";
            this.isEncrypt.Size = new System.Drawing.Size(126, 17);
            this.isEncrypt.TabIndex = 11;
            this.isEncrypt.Text = "Encrypt message first";
            this.isEncrypt.UseVisualStyleBackColor = true;
            this.isEncrypt.CheckedChanged += new System.EventHandler(this.onEncryptChecked);
            // 
            // getMessageInsert
            // 
            this.getMessageInsert.Location = new System.Drawing.Point(276, 81);
            this.getMessageInsert.Name = "getMessageInsert";
            this.getMessageInsert.Size = new System.Drawing.Size(75, 23);
            this.getMessageInsert.TabIndex = 10;
            this.getMessageInsert.Text = "Browse";
            this.getMessageInsert.UseVisualStyleBackColor = true;
            this.getMessageInsert.Click += new System.EventHandler(this.onGetMessageInsert);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.LSBoptExt);
            this.tabPage2.Controls.Add(this.isDecrypt);
            this.tabPage2.Controls.Add(this.doExtract);
            this.tabPage2.Controls.Add(this.imgExtract);
            this.tabPage2.Controls.Add(this.keyLabelExtract);
            this.tabPage2.Controls.Add(this.getImgExtract);
            this.tabPage2.Controls.Add(this.keyExtract);
            this.tabPage2.Controls.Add(this.imageLabelExtract);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(370, 229);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Extract";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "LSB option";
            // 
            // LSBoptExt
            // 
            this.LSBoptExt.FormattingEnabled = true;
            this.LSBoptExt.Items.AddRange(new object[] {
            "1 bit",
            "2 bit",
            "3 bit"});
            this.LSBoptExt.Location = new System.Drawing.Point(6, 129);
            this.LSBoptExt.Name = "LSBoptExt";
            this.LSBoptExt.Size = new System.Drawing.Size(121, 21);
            this.LSBoptExt.TabIndex = 22;
            this.LSBoptExt.SelectedIndexChanged += new System.EventHandler(this.onLSBOptExtChanged);
            // 
            // isDecrypt
            // 
            this.isDecrypt.AutoSize = true;
            this.isDecrypt.Location = new System.Drawing.Point(133, 131);
            this.isDecrypt.Name = "isDecrypt";
            this.isDecrypt.Size = new System.Drawing.Size(150, 17);
            this.isDecrypt.TabIndex = 21;
            this.isDecrypt.Text = "Decrypt resulting message";
            this.isDecrypt.UseVisualStyleBackColor = true;
            this.isDecrypt.CheckedChanged += new System.EventHandler(this.onDecryptChecked);
            // 
            // doExtract
            // 
            this.doExtract.Location = new System.Drawing.Point(276, 81);
            this.doExtract.Name = "doExtract";
            this.doExtract.Size = new System.Drawing.Size(75, 23);
            this.doExtract.TabIndex = 19;
            this.doExtract.Text = "Extract";
            this.doExtract.UseVisualStyleBackColor = true;
            this.doExtract.Click += new System.EventHandler(this.onExtract);
            // 
            // imgExtract
            // 
            this.imgExtract.Location = new System.Drawing.Point(6, 34);
            this.imgExtract.Name = "imgExtract";
            this.imgExtract.Size = new System.Drawing.Size(264, 20);
            this.imgExtract.TabIndex = 11;
            // 
            // keyLabelExtract
            // 
            this.keyLabelExtract.AutoSize = true;
            this.keyLabelExtract.Location = new System.Drawing.Point(6, 65);
            this.keyLabelExtract.Name = "keyLabelExtract";
            this.keyLabelExtract.Size = new System.Drawing.Size(53, 13);
            this.keyLabelExtract.TabIndex = 15;
            this.keyLabelExtract.Text = "Seed Key";
            // 
            // getImgExtract
            // 
            this.getImgExtract.Location = new System.Drawing.Point(276, 34);
            this.getImgExtract.Name = "getImgExtract";
            this.getImgExtract.Size = new System.Drawing.Size(75, 23);
            this.getImgExtract.TabIndex = 13;
            this.getImgExtract.Text = "Browse";
            this.getImgExtract.UseVisualStyleBackColor = true;
            this.getImgExtract.Click += new System.EventHandler(this.onGetImageExtract);
            // 
            // keyExtract
            // 
            this.keyExtract.Location = new System.Drawing.Point(6, 81);
            this.keyExtract.Name = "keyExtract";
            this.keyExtract.Size = new System.Drawing.Size(261, 20);
            this.keyExtract.TabIndex = 14;
            // 
            // imageLabelExtract
            // 
            this.imageLabelExtract.AutoSize = true;
            this.imageLabelExtract.Location = new System.Drawing.Point(6, 18);
            this.imageLabelExtract.Name = "imageLabelExtract";
            this.imageLabelExtract.Size = new System.Drawing.Size(55, 13);
            this.imageLabelExtract.TabIndex = 12;
            this.imageLabelExtract.Text = "Image File";
            // 
            // openFileMsgIns
            // 
            this.openFileMsgIns.FileName = "messageIns";
            // 
            // openImageExt
            // 
            this.openImageExt.FileName = "imageExt";
            // 
            // labelPSNR
            // 
            this.labelPSNR.AutoSize = true;
            this.labelPSNR.Location = new System.Drawing.Point(35, 436);
            this.labelPSNR.Name = "labelPSNR";
            this.labelPSNR.Size = new System.Drawing.Size(79, 13);
            this.labelPSNR.TabIndex = 21;
            this.labelPSNR.Text = "PSNR (in dB) : ";
            // 
            // resImgInsertLabel
            // 
            this.resImgInsertLabel.AutoSize = true;
            this.resImgInsertLabel.Location = new System.Drawing.Point(269, 300);
            this.resImgInsertLabel.Name = "resImgInsertLabel";
            this.resImgInsertLabel.Size = new System.Drawing.Size(83, 13);
            this.resImgInsertLabel.TabIndex = 20;
            this.resImgInsertLabel.Text = "Resulting Image";
            // 
            // resImgInsertView
            // 
            this.resImgInsertView.BackColor = System.Drawing.Color.White;
            this.resImgInsertView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resImgInsertView.Location = new System.Drawing.Point(230, 316);
            this.resImgInsertView.Name = "resImgInsertView";
            this.resImgInsertView.Size = new System.Drawing.Size(163, 107);
            this.resImgInsertView.TabIndex = 19;
            this.resImgInsertView.TabStop = false;
            // 
            // oriImgInsertView
            // 
            this.oriImgInsertView.BackColor = System.Drawing.Color.White;
            this.oriImgInsertView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.oriImgInsertView.Location = new System.Drawing.Point(38, 316);
            this.oriImgInsertView.Name = "oriImgInsertView";
            this.oriImgInsertView.Size = new System.Drawing.Size(163, 107);
            this.oriImgInsertView.TabIndex = 18;
            this.oriImgInsertView.TabStop = false;
            // 
            // oriImgInsertLabel
            // 
            this.oriImgInsertLabel.AutoSize = true;
            this.oriImgInsertLabel.Location = new System.Drawing.Point(79, 300);
            this.oriImgInsertLabel.Name = "oriImgInsertLabel";
            this.oriImgInsertLabel.Size = new System.Drawing.Size(74, 13);
            this.oriImgInsertLabel.TabIndex = 17;
            this.oriImgInsertLabel.Text = "Original Image";
            // 
            // resultSaver
            // 
            this.resultSaver.Location = new System.Drawing.Point(277, 432);
            this.resultSaver.Name = "resultSaver";
            this.resultSaver.Size = new System.Drawing.Size(75, 21);
            this.resultSaver.TabIndex = 22;
            this.resultSaver.Text = "Save";
            this.resultSaver.UseVisualStyleBackColor = true;
            this.resultSaver.Click += new System.EventHandler(this.onSaveImageResult);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 469);
            this.Controls.Add(this.resultSaver);
            this.Controls.Add(this.labelPSNR);
            this.Controls.Add(this.resImgInsertLabel);
            this.Controls.Add(this.resImgInsertView);
            this.Controls.Add(this.oriImgInsertView);
            this.Controls.Add(this.oriImgInsertLabel);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.title);
            this.Name = "Form1";
            this.Text = "StegoPict";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resImgInsertView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oriImgInsertView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.TextBox imgInsert;
        private System.Windows.Forms.Label imgLabelInsert;
        private System.Windows.Forms.Button getImgInsert;
        private System.Windows.Forms.Button doInsert;
        private System.Windows.Forms.OpenFileDialog openImgIns;
        private System.Windows.Forms.TextBox keyStr;
        private System.Windows.Forms.Label keyLabelInsert;
        private System.Windows.Forms.Label msgLabelInsert;
        private System.Windows.Forms.TextBox msgInsert;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button getMessageInsert;
        private System.Windows.Forms.TextBox imgExtract;
        private System.Windows.Forms.Label keyLabelExtract;
        private System.Windows.Forms.Button getImgExtract;
        private System.Windows.Forms.TextBox keyExtract;
        private System.Windows.Forms.Label imageLabelExtract;
        private System.Windows.Forms.Button doExtract;
        private System.Windows.Forms.CheckBox isEncrypt;
        private System.Windows.Forms.CheckBox isDecrypt;
        private System.Windows.Forms.OpenFileDialog openFileMsgIns;
        private System.Windows.Forms.OpenFileDialog openImageExt;
        private System.Windows.Forms.Label labelPSNR;
        private System.Windows.Forms.Label resImgInsertLabel;
        private System.Windows.Forms.PictureBox resImgInsertView;
        private System.Windows.Forms.PictureBox oriImgInsertView;
        private System.Windows.Forms.Label oriImgInsertLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox LSBoptIns;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox LSBoptExt;
        private System.Windows.Forms.Button resultSaver;
        private System.Windows.Forms.SaveFileDialog saveInsImage;
        private System.Windows.Forms.SaveFileDialog saveExtFile;
    }
}

