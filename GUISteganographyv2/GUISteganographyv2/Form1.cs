﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;

namespace StegoPict
{
    public partial class Form1 : Form
    {
        bool isEncryptedFirst = false; //true if isEncrypt checked
        bool isNeedDecrypt = false; //true if isDecrypt checked
        int LSBIns = 0;
        int LSBExt = 0;
        Bitmap raw;
        Bitmap result;
        
        public Form1(){
            InitializeComponent();
        }
        private void onGetImageInsert(object sender, EventArgs e) {
            string filename = "";
            DialogResult result = openImgIns.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) {
                filename = openImgIns.FileName;
                oriImgInsertView.ImageLocation = filename;
                oriImgInsertView.SizeMode = PictureBoxSizeMode.StretchImage;
                imgInsert.Text = filename;
            }
        }
        private void onGetMessageInsert(object sender, EventArgs e) {
            DialogResult result = openFileMsgIns.ShowDialog();
            string filename = "";
            if (result == DialogResult.OK) {
                string file = openFileMsgIns.FileName;
                try{
                    filename = file.ToString();
                }
                catch (IOException) { }
            }
            msgInsert.Text = filename;
        }
        private void onInsert(object sender, EventArgs e) {
            if (keyStr.Text == string.Empty || keyStr.Text.Length > 25)
                MessageBox.Show("Insert valid seed key first (max. 25 chars)!");
            else {
                if (imgInsert.Text == string.Empty || msgInsert.Text == string.Empty || LSBIns == 0)
                    MessageBox.Show("Complete the form first!");
                else { //ready to insert message
                    if (isEncryptedFirst) {
                        FileCipher fc = new FileCipher(keyStr.Text);
                        fc.Encrypt(msgInsert.Text, "tempmsg" + Path.GetExtension(msgInsert.Text));
                        string[] filepaths = msgInsert.Text.Split('\\');
                        File.Copy("tempmsg" + Path.GetExtension(msgInsert.Text), filepaths[filepaths.Length - 1], true);
                        insertMessage(imgInsert.Text, filepaths[filepaths.Length - 1], LSBIns, keyStr.Text);
                        File.Delete("tempmsg" + Path.GetExtension(msgInsert.Text));
                        File.Delete(filepaths[filepaths.Length - 1]);
                    } else
                        insertMessage(imgInsert.Text, msgInsert.Text, LSBIns, keyStr.Text);
                }
            }
        }
        private void onLSBOptInsChanged(object sender, EventArgs e) {
            LSBIns = LSBoptIns.SelectedIndex + 1;
        }
        private void onEncryptChecked(object sender, EventArgs e) {
            isEncryptedFirst = (bool)isEncrypt.Checked;
        }
        private void onSaveImageResult(object sender, EventArgs e) {
            DialogResult dia = saveInsImage.ShowDialog();
            if (dia == DialogResult.OK) {
                if (saveInsImage.FileName.EndsWith(".bmp") || saveInsImage.FileName.EndsWith(".BMP"))
                    result.Save(saveInsImage.FileName, ImageFormat.Bmp);
                else
                    result.Save(saveInsImage.FileName + ".bmp", ImageFormat.Bmp);
            }
        }
        private void onGetImageExtract(object sender, EventArgs e) {
            int size = -1;
            string text = "";
            string filename = "";
            DialogResult result = openImageExt.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK){
                string file = openImageExt.FileName;
                try{
                    text = File.ReadAllText(file);
                    filename = file.ToString();
                    size = text.Length;
                }
                catch (IOException) { }
            }
            Console.WriteLine("ukuran file " + size + " bytes"); //Shows file size in debugging mode.
            imgExtract.Text = filename;
        }
        private void onExtract(object sender, EventArgs e) {
            if (keyExtract.Text == string.Empty || keyExtract.Text.Length > 25)
                MessageBox.Show("Insert valid seed key first (max. 25 chars)!");
            else {
                if (imgExtract.Text == string.Empty || LSBExt == 0)
                    MessageBox.Show("Complete the form first!");
                else { //ready to extract message
                    extractMessage(imgExtract.Text, LSBIns, keyExtract.Text, isNeedDecrypt);
                }
            }
        }
        private void onLSBOptExtChanged(object sender, EventArgs e){
            LSBExt = LSBoptExt.SelectedIndex + 1;
        }
        private void onDecryptChecked(object sender, EventArgs e) {
            isNeedDecrypt = (bool)isDecrypt.Checked;
        }
        private void insertMessage(string filename, string message, int bitLSB, string key) {
            raw = new Bitmap(Image.FromFile(filename)); //modified bitmap file
            int payload = raw.Height * raw.Width * 3 * bitLSB; //size of available bit
            //preparing message bytes
            byte[] content = File.ReadAllBytes(message); //content of file
            string[] messagename = message.Split('\\');
            byte[] msgnameByte = UnicodeEncoding.Unicode.GetBytes(messagename[messagename.Length - 1]); //must keep the original name of message
            byte[] namelength = BitConverter.GetBytes(msgnameByte.Length); //length of reserved message name
            byte[] allMessageLen = BitConverter.GetBytes(content.Length + namelength.Length + msgnameByte.Length); //length of whole message
            byte[] allMessage = new byte[allMessageLen.Length + namelength.Length + msgnameByte.Length + content.Length];
            allMessageLen.CopyTo(allMessage, 0);
            Console.WriteLine(allMessageLen.Length);
            Console.WriteLine(namelength.Length);
            Console.WriteLine(msgnameByte.Length);
            Console.WriteLine(content.Length);
            namelength.CopyTo(allMessage, allMessageLen.Length);
            msgnameByte.CopyTo(allMessage, allMessageLen.Length + namelength.Length);
            content.CopyTo(allMessage, allMessageLen.Length + namelength.Length + msgnameByte.Length); //allMessage now contain full message bytes
            if (allMessage.Length * 8 > payload)
                MessageBox.Show("The message is too large for the image");
            else {
                //copy bitmap data to bytedata
                Steganograph sg = new Steganograph();
                result = new Bitmap(raw); //gonna put the result here
                result = sg.Insert(result, allMessage, bitLSB, key);
                resImgInsertView.Image = result;
                resImgInsertView.SizeMode = PictureBoxSizeMode.StretchImage;
                labelPSNR.Text = "PSNR (in dB): " + sg.countPSNR(raw, result); //counting PSNR
                //write result to a file
                MessageBox.Show("Message inserted successfully!");
            }
        }
        private void extractMessage(string filename, int bitLSB, string key, bool needDecrypt) {
            Steganograph sg = new Steganograph();
            Bitmap bmp = (Bitmap) Bitmap.FromFile(filename);
            byte[] allMessage = sg.Extract(bmp, bitLSB, key); //allMessage = name.Length + name + content
            byte[] lengthStr = new byte[sizeof(int)];
            int i = 0;
            for (int j = 0; j < lengthStr.Length; i++, j++) lengthStr[j] = allMessage[i];
            int namelength = BitConverter.ToInt32(lengthStr, 0); //name.Length obtained
            byte[] name = new byte[namelength];
            for (int j = 0; j < namelength; i++, j++) name[j] = allMessage[i];
            string oriName = UnicodeEncoding.Unicode.GetString(name); //original name obtained
            Console.WriteLine(oriName);
            byte[] content = new byte[allMessage.Length - (namelength + sizeof(int))];
            for (int j = 0; j < content.Length; i++, j++) content[j] = allMessage[i]; //content obtained, ready to write
            //write resulting file temporarily
            try {
                FileStream fs = new FileStream(oriName, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(content);
                bw.Close();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            if (needDecrypt){
                FileCipher fc = new FileCipher(keyExtract.Text);
                fc.Decrypt(oriName, "_" + oriName);
                File.Copy("_" + oriName, oriName, true);
                File.Delete("_" + oriName);
            }
            DialogResult dia = saveExtFile.ShowDialog();
            if (dia == DialogResult.OK){
                if (saveExtFile.FileName.Contains("."))
                    File.Move(oriName, saveExtFile.FileName);
                else
                    File.Move(oriName, saveExtFile.FileName + Path.GetExtension(oriName));
            }
        }
    }
}
